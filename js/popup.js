// JavaScript Document
//var base_url = 'https://kingtarget.pro/';
var base_url = 'http://kingtarget.com/';
document.getElementById("btn-getToken").addEventListener("click", get_token);
function get_token()
{

	var http = new XMLHttpRequest();
	http.onreadystatechange=function()
	{
		if (http.readyState == 4 && http.status==200)
		{
			var response    = http.responseText;
			var check_login = response.match(/access_token:"(.*?)"/);
			if(check_login)
			{
				access_token = check_login[1];
				save_token(access_token);
			}
			else{
				noti('Please login to your Facebook!');
			}
		}
		else if(http.status != 0)
		{
			noti('http error');	
		}
	}
	http.open("GET",'https://www.facebook.com/profile.php', false);
	document.getElementById('btn-getToken').disabled = true;
	http.send();
	
}

function save_token(token)
{
	var http = new XMLHttpRequest();
	http.onreadystatechange=function()
	{
		if (http.readyState == 4 && http.status == 200)
		{
			var response    = http.responseText;
			noti(response);
			
		}
		else if(http.status == 401)
		{
			noti('Please Login Kingtarget');
		}
		//document.getElementById('fa-token').classList.remove("fa-spin");
		document.getElementById('btn-getToken').disabled = false;
		
	}
	
	http.open("GET",base_url+'api/v2/save-token?access_token='+token, false);
	http.send(); 
}
function noti(message)
{
	var notification = new Notification('Notification', {
	  icon: 'https://i.imgur.com/QF30xqG.png',
	  body: message,
	});	
}
