var run = 0;
//var base_url = 'http://target.app/';
var base_url = 'https://kingtarget.pro/';
var notification;
var access_token;
var group_id;
var result_page  = {};
var result_group = {};
var result_info = {
	'gender':[],
	'location':{'city':[],'country':[]},
	'hometown':{'city':[],'country':[]},
	'age':[],
	'relationship':[],
};
var max_page = 2000;
var cur_page;
var uid_arr;
var uid = '';
var tid = 0;
var kid = 0;
var path_page;
var path_group;
var scanning = 0;



$(document).ready(function(e){
    
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) 
{
	if(request && request.getToken == 1)
	{
		sendResponse({token: access_token});
	}
	
}); 

chrome.browserAction.onClicked.addListener(function(activeTab){
  chrome.tabs.create({ url: base_url });
});
	
	
chrome.runtime.onMessageExternal.addListener(function(request, sender, sendResponse)
{
	
	if(request && request.message == "version")
	{
		sendResponse({'version': '1.0.4'});
	}
	
	if(request && request.message == "duration")
	{
		sendResponse({'group_id':group_id,'done':kid,'total':tid});
	}
	
	if(request && request.adssuccess == 1){
		notice('Setting audience analytics success!','https://kingtarget.pro/app/dashboard/');	
	}
	
	if(request && request.scan == 1 && run == 0)
	{
		if(!access_token)
		{
			notice('Error! Please login Facebook','https://facebook.com/');
		}
		else{
			run = 1;
			group_id  = request.group_id;	
			start_scan(group_id);		
		}
	}
	
	
});

get_token();

function get_token()
{
	$.ajax({
		url:'https://www.facebook.com/profile.php',
		beforeSend: function(){},
		success:function(res){
			check_login = res.match(/access_token:"(.*?)"/);
			if(check_login)
			{
				access_token = check_login[1];
				
			}
			else{
				setTimeout(function(){
					get_token();	
				},3000);	
			}
		},
		error:function(xhr){
			setTimeout(function(){
				get_token();	
			},3000);
		}	
	});	
}


function start_scan(group)
{
	
	$.ajax({
		url:base_url+'api/ajax/scan',
		data:{'group_id':group},
		type:'post',
		dataType:"json",
		success:function(res)
		{	
			if(res.error){
				notice(res.message);
				run = 0;
				return false;	
			}
			group_id = group;
			uid_arr  = res.uid;
			tid      = uid_arr.length;
			kid      = 0;
			cur_page = 0;		
			get_user_info();	
		},	
	});	
}


function get_user_info()
{
	if(kid >= tid){
		complete();	
		return false;
	}
	
	uid = uid_arr[kid];
	$.ajax({
		url:'https://graph.facebook.com/'+uid+'?fields=id,birthday,gender,hometown{location},location{location},relationship_status&access_token='+access_token,
		dataType:"json",
		success:function(res)
		{
			
			if(res.gender)
				result_info.gender.push(res.gender);
				
			if(res.relationship_status)
				result_info.relationship.push(res.relationship_status);	
			//Hometown	
			if(res.hometown && res.hometown.location && res.hometown.location.city)
				result_info.hometown.city.push(res.hometown.location.city.replace('.',' '));
			if(res.hometown && res.hometown.location && res.hometown.location.country)
				result_info.hometown.country.push(res.hometown.location.country.replace('.',' '));
				
			//Location	
			if(res.location && res.location.location && res.location.location.city)
				result_info.location.city.push(res.location.location.city.replace('.',' '));
			if(res.location && res.location.location && res.location.location.country)
				result_info.location.country.push(res.location.location.country.replace('.',' '));
				
			//Age range
			if(res.birthday){
				birthday = res.birthday.split('/');
				if(birthday.length > 2)
					result_info.age.push(2017 - birthday[2]);	
			}
			
			path_page = '';
			path_group = '';
			get_page();					
		},	
		error:function(xhr)
		{
			if(xhr.responseJSON.error.code == 190){
				
				notice('Please restart extension!');
				
			}
			else{
				next_user();
				console.log(xhr);
			}
		}
	});	
}

function next_user()
{
	cur_page = 0;
	kid++;
	get_user_info();	
}

function get_page()
{
	if(!path_page)
	  path_page ='https://graph.facebook.com/v2.9/'+uid+'/likes?fields=id,name,fan_count,category,picture{url}&limit=100&access_token='+access_token;
	  
	$.ajax({
		url : path_page,
		dataType:"json",
		success:function(res)
		{
			if(res.data && res.data.length > 0)
			{
				$.each(res.data,function(k,row){
					if(!(row.id in result_page))
					{
						row['total'] = 1;
						result_page[row.id] = row;
					}	
					else
						result_page[row.id]['total'] += 1;
				});	
			}
			
			cur_page += 100;
			if(cur_page >= max_page)
			{
				next_user();
				return false;	
			}
			
			if(res.paging && res.paging.next){
				path_page = res.paging.next;	
				setTimeout(function(){
					get_page();
				},200);
				
			}
			else	
			{
				next_user();
			}	
		},	
		error:function(res)
		{
			if(xhr.responseJSON.error.code == 190){
				notice('Please restart extension!');
			}
			else{
				get_page();
			}
			
		}
	});
}

function get_group()
{
	if(!path_group)
	  path_group ='https://graph.facebook.com/'+uid+'/groups?fields=id,name,picture{url},members.limit(0).summary(true)&access_token='+access_token;
	  
	$.ajax({
		url : path_group,
		dataType:"json",
		success:function(res){
			
			if(res.data && res.data.length > 0)
			{
				$.each(res.data,function(k,row)
				{
					if(!(row.id in result_group))
					{
						row['total'] = 1;
						row['members'] = row.members.summary.total_count;
						result_group[row.id] = row;
					}	
					else
						result_group[row.id]['total'] += 1;
				});	
			}
			
			if(res.paging && res.paging.next)
			{
				path_group = res.paging.next;
				setTimeout(function(){
					get_group();
				},200);
			}
			
			else	
			{
				kid++;
				get_user_info();
			}	
		},	
		error:function(res){
			get_page();
			
		}
	});
}



function complete()
{
	var page_tmp = [];
	var group_tmp = [];
	var interestValid = [];
	
	$.each(result_page,function(k,row)
	{
		if(row.total > 1){
			page_tmp.push([row,row.total,row.name]);
		}
	});
	
	$.each(result_group,function(k,row)
	{
		if(row.total > 1){
			group_tmp.push([row,row.total]);
			
		}
	});
	
	page_tmp.sort(function(a, b) {
		return b[1] - a[1]
	});
	
	group_tmp.sort(function(a, b) {
		return b[1] - a[1]
	});

	page_ngon  = page_tmp.slice(0,400);
	group_ngon = group_tmp.slice(0,100);
	
	result_page  = {};
	result_group = {};
	
	delete page_tmp;
	delete group_tmp;
	
	var interestValid = page_ngon.map(function(value,index) { return value[2]; });
	interestValid.push('fast & furiust');
	
	$.ajax({
		url:'https://graph.facebook.com/v2.10/search',
		data:{
			'access_token':access_token,
			'type':'adInterestValid',
			'ignore_case':'true',
			'interest_list':JSON.stringify(interestValid)
		},
		dataType:"json",
		success: function(res)
		{	
			
			var interest = [];
			$.each(res.data,function(k,row){
				if(row.valid !== true)
					return false;
				interest.push(row.name);
			});
					
			$.ajax({
				url:base_url+'api/ajax/save',
				type:'post',
				data:{'group_id':group_id,'info':JSON.stringify(result_info),'page':JSON.stringify(page_ngon),'group':JSON.stringify(group_ngon),'interest':JSON.stringify(interest)},
				beforeSend: function()
				{
					delete page_ngon;
					delete group_ngon;
					delete interestValid;
					
					result_info = {
						'gender':[],
						'location':{'city':[],'country':[]},
						'hometown':{'city':[],'country':[]},
						'age':[],
						'relationship':[]
					};
				},
				success: function(res)
				{	
					group_id = '';					
					if(res){
						setTimeout(function(){
							start_scan(res)
						},3000);	
					}
					else 
						run = 0;
				},
				error: function(res){
					run = 0;	
				}
			});
		}	
	});
	
}

function notice(message,redirect)
{
	var notification = new Notification('Notification', {
	  icon: 'https://i.imgur.com/PrMQ0GA.png',
	  body: message,
	});
	
	if(redirect)
	{
		notification.onclick = function () {
		  window.open(redirect);      
		};
	}
	
}


});